import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import { createStore } from 'vuex'

import firebase from "firebase/app"
import 'firebase/auth';
import 'firebase/database';


const store = createStore({
  state () {
    return{
      carts: [1,2,3,4,5,1,1,1,1],
      showPopupVisibly:false,
      logsucces: false,
      videos:[
        {
          id:1,
          link:"https://www.youtube.com/embed/8JLIJoIJpEU",
          counter: 0,
          clicked:false,
          },
        {
          id:2,
          link:"https://www.youtube.com/embed/CBy3P30Ur10",
          counter: 0,
          clicked:false,},
          
        {
          id:3,
          link:"https://www.youtube.com/embed/yAzc9qn3rz0",
          counter: 0,
          clicked:false,},
        {
          id:4,
          link:"https://www.youtube.com/embed/tOQG_A5uPFI",
          counter: 0,
          clicked:false,},
        {
          id:5,
          link:"https://www.youtube.com/embed/n6nZNKQGldk",
          counter: 0,
          clicked:false,}
    ],
    }
  },
  getters:{
    getCarts(state) {
      return state.carts;
    },
    getVideos(state){
      return state.videos;
    },
    getShow(state){
      return state.showPopupVisibly;
    },
    getLog(state){
      return state.logsucces;
    }
  },
  mutations: {
    setOpen (state) {
      state.showPopupVisibly = !state.showPopupVisibly;
    },
    closeOpen (state) {
      state.showPopupVisibly = false;
    },
    closeBTH (state) {
      state.logsucces = true;
    },
    Outuser (state) {
      state.logsucces = false;
    }
  }
})

const firebaseConfig = {
    apiKey: "AIzaSyAUp3b1XherOeFIXqNOIRrN0P6HdcoJcPI",
    authDomain: "vue-tiktok.firebaseapp.com",
    projectId: "vue-tiktok",
    storageBucket: "vue-tiktok.appspot.com",
    messagingSenderId: "570882673610",
    appId: "1:570882673610:web:8ac1d924a087eb2caa1d33",
    measurementId: "G-1CJ2ZX3HSW"
};

firebase.initializeApp(firebaseConfig);
createApp(App).use(router).use(store).mount('#app')