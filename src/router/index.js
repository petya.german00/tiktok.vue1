import { createRouter, createWebHistory } from 'vue-router';
import subscribe from "../pages/subscribe.vue";
import home from "../pages/home"


const routes = [
  {
    path: '/sub',
    name:"subscribe",
    component: subscribe
  },
  {
    path: '/',
    name:"home",
    component: home
  }
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
});

export default router;
